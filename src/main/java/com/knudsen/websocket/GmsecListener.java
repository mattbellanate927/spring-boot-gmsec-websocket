/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.knudsen.websocket;

import gov.nasa.gsfc.gmsec.api.Config;
import gov.nasa.gsfc.gmsec.api.GMSEC_Exception;
import gov.nasa.gsfc.gmsec.api.Message;
import gov.nasa.gsfc.gmsec.api.mist.ConnectionManager;
import gov.nasa.gsfc.gmsec.api.mist.ConnectionManagerCallback;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class GmsecListener {
    Config cfg;
    ConnectionManager cm;
        
    @Autowired
    SimpMessagingTemplate template;
    
    public GmsecListener() {
        try {
            cfg = new Config();
            cfg.addValue("mw-id", "bolt");

            cm = new ConnectionManager(cfg);
            cm.initialize();
            cm.startAutoDispatch();
            
            cm.subscribe("GMSEC.>", new GmsecMessagePublisher());
        }
        catch (GMSEC_Exception | IllegalArgumentException e) {
            Logger.getLogger("GmsecListener").log(Level.WARNING, e.toString());
        }
    }
    
    private class GmsecMessagePublisher extends ConnectionManagerCallback {


        @Override
        public void onMessage(ConnectionManager cm, Message msg) {
            template.convertAndSend("/topic/messages", new GmsecMessage(msg.toJSON()));
        }
    }
}