package com.knudsen.websocket;

/**
 *
 * @author stknudse
 */
public class GmsecMessage {
    
    private String message;
    
    public GmsecMessage(String jsonString) {
        this.message = jsonString;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
