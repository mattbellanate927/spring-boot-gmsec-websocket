# Spring Boot GMSEC Messages Over WebSocket/STOMP

This is a quick experiment that forwards messages from a local GMSEC Bolt Bus
to a websocket for consumption by a web browser.

The implementation is rather naive, given that there is absolutely no buffering
of messages in the event of a network slowdown.
